#include <iostream>

using namespace std;

void display(string name, int count, int price)
{
    cout << name << "\t\t: " << count << " = Rp. " << price << endl;
}

int main()
{
    char name[100];
    int cpu_count, monitor_count, keyboard_count, mouse_count, printer_count, cpu_price = 2000000, monitor_price = 2400000, keyboard_price = 200000, mouse_price = 150000, printer_price = 850000;

    cout << "\t\t\t*** Selamat datang di Toko ABC ***" << endl << endl;

    cout << "Masukan nama anda : ";

    cin.get(name, 100);

    cout << endl;

    cout << "Hai " << name << endl;

    cout << "Daftar pemesanan [0 jika tidak ingin memesan] : " << endl;

    cout << "CPU\t\tRp. 2.000.000\t: "; cin >> cpu_count;

    cout << "Monitor\t\tRp. 2.400.000\t: "; cin >> monitor_count;

    cout << "Keyboard\tRp. 200.000\t: "; cin >> keyboard_count;

    cout << "Mouse\t\tRp. 150.000\t: "; cin >> mouse_count;

    cout << "Printer\t\tRp. 850.000\t: "; cin >> printer_count;

    cout << endl << endl;

    // Total CPU price.
    int cpu_total       = cpu_price * cpu_count;

    // Total monitor price.
    int monitor_total   = monitor_price * monitor_count;

    // Total keyboard price.
    int keyboard_total  = keyboard_price * keyboard_count;

    // Total mouse price.
    int mouse_total     = mouse_price * mouse_count;

    // Total printer price.
    int printer_total   = printer_price * printer_count;

    // Grand total
    int total           = cpu_total + monitor_total + keyboard_total + mouse_total + printer_total;

    cout << "Rincial pembelian " << endl;

    display("CPU\t", cpu_count, cpu_total);
    display("Monitor ", monitor_count, monitor_total);
    display("Keyboard", keyboard_count, keyboard_total);
    display("Mouse\t", mouse_count, mouse_total);
    display("Printer\t", printer_count, printer_total);

    cout << endl << endl;

    cout << "Total" "\t\t: Rp. " << total << endl << endl;

    cout << "Terima kasih telah berbelanja di toko kami. " << endl;

    return 0;
}
